{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE Trustworthy       #-}


module SurveyMonkey.Types where

import           Control.Lens.Getter

import           Control.Lens.TH
import           Data.Aeson
import           Data.Aeson.Types    as Aeson
import           Data.Default
import           Data.Text           (Text)
import qualified Data.Text           as T
import           Data.Time.Clock
import           Data.Time.Format
import qualified Data.Vector         as Vec
import           GHC.Generics        (Generic)
import           Network.HTTP.Client hiding (Response)



-- | the api key to use
type ApiKey = Text

-- | the api authentication to use
type Auth = Text

-- | a config / state object
data SurveyMonkeyConfig =
  SurveyMonkeyConfig {
    _apiKey      :: ApiKey,
    _auth        :: Auth,
    _httpManager :: Manager
    }

-- | the error type for the api
data SurveyMonkeyError
  = SurveyMonkeyOverRate Int -- ^ we are over out api quota. wait i seconds.
  | SurveyMonkeyParseError String -- ^ there was a problem parsing the response
  | SurveyMonkeyHTTPException HttpException -- ^ an 'HttpException' happend
  | SurveyMonkeyNotAuthenticated -- ^ not authenticated
  | SurveyMonkeyInvalidUserCredentials -- ^ invalid user credentials
  | SurveyMonkeyIvalidRequest -- ^ an invalid request
  | SurveyMonkeyUnknownUser -- ^ unknown user
  | SurveyMonkeySystemError -- ^ a system error on surveymonkey side
    deriving (Show)

type SurveyMonkeyResult = Either SurveyMonkeyError


newtype SurveyId = SurveyId Int deriving (Show, Eq, Ord)

newtype QuestionId = QuestionId Int deriving (Show, Eq, Ord)

newtype PageId = PageId Int deriving (Show, Eq, Ord)

newtype AnswerId = AnswerId Int deriving (Show, Eq, Ord)

newtype CollectorId = CollectorId Int deriving (Show, Eq, Ord)

newtype RespondentId = RespondentId Int deriving (Show, Eq, Ord)


data PageMeta =
  PageMeta {
    _pageMetaPage     :: Maybe Int,
    _pageMetaPageSize :: Maybe Int
    } deriving (Show, Eq)

makeClassy ''PageMeta


data SurveyListParams =
  SurveyListParams {
  _surveyListParamsPageMeta    :: PageMeta,
    _surveyListParamsTitle     :: Maybe Text,
    _surveyListParamsStartDate :: Maybe UTCTime,
    _surveyListParamsEndDate   :: Maybe UTCTime
    } deriving (Show, Eq, Generic)


data Survey =
  Survey {
    _surveySurveyId        :: ! SurveyId,
    _surveyTitle           :: ! Text,
    _surveyAnalysisUrl     :: ! Text,
    _surveyPreviewUrl      :: ! Text,
    _surveyCustomVariables :: ! [ (Text, QuestionId) ],
    _surveyDateCreated     :: ! UTCTime,
    _surveyDateModified    :: ! UTCTime,
    _surveyLanguageId      :: ! Int,
    _surveyNickname        :: ! Text,
    _surveyPages           :: ! [Page]
  } deriving (Show, Eq, Generic)


type SurveyIdList = SMList SurveyId


data SMList t =
  SMList { _smList         :: ! [t]
         , _smListPage     :: Int
         , _smListPageSize :: Int
         } deriving (Show)

data Page =
  Page {
  _pageId         :: PageId,
  _pageHeading    :: Text,
  _pageSubHeading :: Maybe Text,
  _pageQuestions  :: [Question]
  } deriving (Show, Eq, Generic)


data Question =
  Question {
  _questionId       :: ! QuestionId,
  _questionType     :: ! QuestionType,
  _questionHeading  :: ! Text,
  _questionPosition :: ! Int,
  _questionAnswers  :: ! [ Answer ]
  } deriving (Show, Eq, Generic)


data QuestionType =
  QuestionType {
   _questionTypeFamily  :: ! Text,
   _questionTypeSubtype :: ! Text,
   _questionTypeName    :: ! Text
   } deriving (Show, Eq, Generic)



data Answer =
  Answer {
   _answerId       :: ! AnswerId,
   _answerVisible  :: ! Bool,
   _answerPosition :: ! (Maybe Int),
   _answerType     :: ! Text,
   _answerText     :: ! (Maybe Text)
   } deriving (Eq, Show, Generic)


data RespondentListParams =
  RespondentListParams {
    _respondentListParamsSurveyId         :: SurveyId,
    _respondentListParamsCollectorId      :: Maybe CollectorId,
    _respondentListParamsPageMeta         :: PageMeta,
    _respondentListParamsStartDate        :: Maybe UTCTime,
    _respondentListParamsEndDate          :: Maybe UTCTime,
    _respondentListParamsStartModfiedDate :: Maybe UTCTime,
    _respondentListParamsEndModifiedDate  :: Maybe UTCTime,
    _respondentListParamsOrderBy          :: Maybe Text,
    _respondentListParamsOrderAsc         :: Maybe Bool,
    _respondentListParamsFields           :: [Text]
    } deriving (Eq, Show, Generic)

defaultRespondentListParams :: SurveyId  -> RespondentListParams
defaultRespondentListParams sid =
  RespondentListParams sid Nothing (PageMeta Nothing Nothing)
  Nothing Nothing Nothing Nothing Nothing Nothing
  ["date_start", "date_modified", "collector_id", "status" ]


data Respondent =
  Respondent {
    _respondentRespondentId :: RespondentId,
    _respondentDateStart    :: Maybe UTCTime,
    _respondentDateModified :: Maybe UTCTime,
    _respondentCollectorId  :: Maybe CollectorId,
    _respondentStatus       :: Maybe Text
  } deriving (Eq, Show, Generic)


type RespondentList = SMList Respondent

data CollectorListParams =
  CollectorListParams {
   _collectorListParamsSurveyId  :: SurveyId,
   _collectorListParamsPageMeta  :: PageMeta,
   _collectorListParamsStartDate :: Maybe UTCTime,
   _collectorListParamsEndDate   :: Maybe UTCTime,
   _collectorListParamsFields    :: [Text]
   } deriving (Eq, Show, Generic)


defaultCollectorListParams :: SurveyId -> CollectorListParams
defaultCollectorListParams sid =
  CollectorListParams sid (PageMeta Nothing Nothing) Nothing Nothing
  ["url", "open", "type", "name", "date_created", "date_modified"]


data Collector =
  Collector {
    _collectorId              :: ! CollectorId,
    _collectorDateCreated     :: ! (Maybe UTCTime),
    _collectorDateModified    :: ! (Maybe UTCTime),
    _collectorName            :: ! (Maybe Text),
    _collectorOpen            :: ! (Maybe Bool),
    _collectorType            :: ! (Maybe Text),
    _collectorUrl             :: ! (Maybe Text),
    _collectorThankYouMessage :: ! (Maybe Text),
    _collectorRedirectUrl     :: ! (Maybe Text)
    } deriving (Eq, Show, Generic)

type CollectorList = SMList Collector


data ResponsesParams =
  ResponsesParams {
    _responsesListParamsRespondentIds :: [ RespondentId ],
    _responsesListParamsSurveyId      :: SurveyId
    } deriving (Eq, Show, Generic)


data ResponseAnswer =
  ResponseAnswer {
    _responseAnswerRow       :: Maybe AnswerId,
    _responseAnswerCol       :: Maybe AnswerId,
    _responseAnswerColChoice :: Maybe AnswerId,
    _responseAnswerText      :: Maybe Text
    } deriving (Eq, Show, Generic)

data ResponseQuestion =
  ResponseQuestion {
    _responseQuestionId      :: QuestionId,
    _responseQuestionAnswers :: [ ResponseAnswer ]
    } deriving (Eq, Show, Generic)

data Response =
  Response {
    _respondentId     :: RespondentId,
    _responseQuestions :: [ ResponseQuestion ]
    } deriving (Eq, Show, Generic)





makeLenses ''SurveyMonkeyConfig
makeLenses ''SMList
makeLenses ''SurveyListParams
makeLenses ''Survey
makeLenses ''Page
makeLenses ''Question
makeLenses ''Collector
makeLenses ''CollectorListParams
makeLenses ''QuestionType
makeLenses ''Answer
makeLenses ''RespondentListParams
makeLenses ''Respondent
makeLenses ''ResponsesParams
makeLenses ''ResponseAnswer
makeLenses ''ResponseQuestion
makeLenses ''Response
makePrisms ''SurveyId
makePrisms ''QuestionId
makePrisms ''PageId
makePrisms ''AnswerId
makePrisms ''CollectorId
makePrisms ''RespondentId

instance HasPageMeta SurveyListParams where
  pageMeta = surveyListParamsPageMeta

instance HasPageMeta RespondentListParams where
  pageMeta = respondentListParamsPageMeta

instance HasPageMeta CollectorListParams where
  pageMeta = collectorListParamsPageMeta

instance FromJSON Collector where
  parseJSON v@(Object o) = do
    Collector <$> parseJSON v
      <*> parseSMDate o "date_created"
      <*> parseSMDate o "date_modified"
      <*> o .:? "name"
      <*> o .:? "open"
      <*> o .:? "type"
      <*> o .:? "url"
      <*> o .:? "thanky_you_message"
      <*> o .:? "redirect_url"
  parseJSON invalid = typeMismatch "Collector" invalid

instance HasHttpManager SurveyMonkeyConfig where
  getHttpManager = view httpManager



parseSMDate :: Object -> Text -> Parser (Maybe UTCTime)
parseSMDate o t = do
  v <- o .:? t
  case v of
    (Just (Aeson.String s)) ->
      return $ parseTimeM True defaultTimeLocale "%F %T" . T.unpack $ s
    (Just invalid) -> typeMismatch "SMDate" invalid
    Nothing -> return Nothing

formatSMDate :: (FormatTime t) => t -> Text
formatSMDate = T.pack . formatTime defaultTimeLocale "%F %T"

instance FromJSON Answer where
  parseJSON v@(Object o) =
    Answer <$> parseJSON v
           <*> o .:"visible"
           <*> o .:? "position"
           <*> o .: "type"
           <*> o .:? "text"
  parseJSON invalid = typeMismatch "QuestionType" invalid



instance FromJSON Question where
  parseJSON v@(Object o) = do
    Question <$> parseJSON v
             <*> o .: "type"
             <*> o .: "heading"
             <*> o .: "position"
             <*> o .: "answers"
  parseJSON invalid = typeMismatch "Question" invalid



instance FromJSON QuestionType where
  parseJSON (Object o) =
    QuestionType <$> o .: "family" <*> o .:"subtype" <*> o .: "name"
  parseJSON invalid = typeMismatch "QuestionType" invalid


instance FromJSON Page where
  parseJSON v@(Object o) = do

    Page <$> parseJSON v
         <*> o .: "heading"
         <*> o .: "sub_heading"
         <*> o .: "questions"
  parseJSON invalid = typeMismatch "Page" invalid


isLastPage :: Getter (SMList t) Bool
isLastPage = to $ \l ->
  (l ^. smListPageSize) > (length $ l ^. smList)


parseSMList :: FromJSON t => Text -> Value -> Parser (SMList t)
parseSMList l (Object v) =
  SMList <$> v .: l <*> v .: "page" <*> v .: "page_size"
parseSMList _ invalid = typeMismatch "SMList" invalid


instance FromJSON (SMList SurveyId) where
  parseJSON = parseSMList "surveys"

instance FromJSON (SMList Collector) where
  parseJSON = parseSMList "collectors"

instance FromJSON (SMList Respondent) where
  parseJSON = parseSMList "respondents"



instance FromJSON Survey where
  parseJSON v@(Object o) = do
    title <- withObject "title" (.: "text" ) <$> (o .: "title")
    cvars <- withArray "custom_variables" parseCustomVariables <$> (o .: "custom_variables")
    date_created <- parseSMDate o "date_created"
    date_modified <- parseSMDate o "date_modified"

    Survey <$> parseJSON v
           <*> title
           <*> o .: "analysis_url"
           <*> o .: "preview_url"
           <*> cvars
           <*> maybe (fail "unable to parse date_created") pure date_created
           <*> maybe (fail "unable to parse date_modified") pure date_modified
           <*> o .: "language_id"
           <*> o .: "nickname"
           <*> o .: "pages"
  parseJSON invalid = typeMismatch "Survey" invalid




parseCustomVariables :: Array -> Parser [(Text, QuestionId)]
parseCustomVariables = Prelude.sequence . fmap parseCustomVariable . Vec.toList


parseCustomVariable :: Value -> Parser (Text, QuestionId)
parseCustomVariable v@(Object o) = (,) <$> o .: "variable_label" <*> parseJSON v
parseCustomVariable invalid = typeMismatch "CustomVariable" invalid




instance FromJSON SurveyId where
  parseJSON (Object v) = SurveyId . read <$> v .: "survey_id"
  parseJSON invalid = typeMismatch "SurveyId" invalid

instance ToJSON SurveyId where
  toJSON sid = object [ "survey_id" .= show (sid ^. _SurveyId) ]

instance FromJSON QuestionId where
  parseJSON (Object v) = QuestionId . read <$> v .: "question_id"
  parseJSON invalid = typeMismatch "QuestionId" invalid

instance FromJSON PageId where
  parseJSON (Object v) = PageId . read <$> v .: "page_id"
  parseJSON invalid = typeMismatch "PageId" invalid

instance FromJSON AnswerId where
  parseJSON (Object v) = do
    aid' <- v .: "answer_id"
    let aid = if (aid' == "None") then 0 else read aid'
    return $ AnswerId aid
  parseJSON invalid = typeMismatch "AnswerId" invalid

instance FromJSON CollectorId where
  parseJSON (Object v) = CollectorId . read <$> v .: "collector_id"
  parseJSON invalid = typeMismatch "CollectorId" invalid

instance FromJSON RespondentId where
  parseJSON (Object v) = RespondentId . read <$> v .: "respondent_id"
  parseJSON invalid = typeMismatch "CollectorId" invalid

instance ToJSON RespondentId where
  toJSON sid = object [ "respondent_id" .= show (sid ^. _RespondentId) ]


instance Default SurveyListParams where
  def = SurveyListParams (PageMeta (pure 1) Nothing) Nothing Nothing Nothing


instance ToJSON SurveyListParams where
  toJSON ps =
    object $ mconcat
    [ maybeArg "page" $ ps ^. pageMetaPage
    , maybeArg "page_size" $ ps ^. pageMetaPageSize
    , maybeArg "title" $ ps ^. surveyListParamsTitle
    , maybeArg "start_date" $ formatSMDate <$> ps ^. surveyListParamsStartDate
    , maybeArg "end_date"   $ formatSMDate <$> ps ^. surveyListParamsEndDate
    ]
    where maybeArg l = maybe [] (\v -> pure $ l .= v)

instance ToJSON CollectorListParams where
  toJSON ps =
    object $ mconcat
    [ maybeArg "page"       $ ps ^. pageMetaPage
    , maybeArg "page_size"  $ ps ^. pageMetaPageSize
    , maybeArg "start_date" $ formatSMDate <$> ps ^. collectorListParamsStartDate
    , maybeArg "end_date"   $ formatSMDate <$> ps ^. collectorListParamsEndDate
    , [ "survey_id" .= (show $ ps ^. collectorListParamsSurveyId . _SurveyId )
      , "fields"    .= (ps ^. collectorListParamsFields)
      ]
    ]
    where maybeArg l = maybe [] (\v -> pure $ l .= v)

instance ToJSON ResponsesParams where
  toJSON ps =
    object $ [ "respondent_ids" .=
               ((show . view _RespondentId) <$> ps ^. responsesListParamsRespondentIds)
             , "survey_id" .= (show $ ps ^. responsesListParamsSurveyId . _SurveyId)
             ]


instance ToJSON RespondentListParams where
  toJSON ps =
    object $ mconcat
    [ maybeArg "page" $ ps ^. pageMetaPage
    , maybeArg "page_size" $ ps ^. pageMetaPageSize
    , maybeArg "start_date" $ formatSMDate <$> ps ^. respondentListParamsStartDate
    , maybeArg "end_date"   $ formatSMDate <$> ps ^. respondentListParamsEndDate
    , maybeArg "start_modified_date" $
      formatSMDate <$> ps ^. respondentListParamsStartModfiedDate
    , maybeArg "end_modified_date" $
      formatSMDate <$> ps ^. respondentListParamsEndModifiedDate
    , maybeArg "order_by" $ ps ^. respondentListParamsOrderBy
    , maybeArg "order_asc" $ ps ^. respondentListParamsOrderAsc
    , maybeArg "fields" $
        case ps ^. respondentListParamsFields of
          [] -> Nothing
          fs -> pure fs
    , [ "survey_id" .= (show $ ps ^. respondentListParamsSurveyId . _SurveyId ) ]
    ]
    where maybeArg l = maybe [] (\v -> pure $ l .= v)




instance FromJSON ResponseAnswer where
  parseJSON (Object o) = do
    row' <- o .:? "row"
    col' <- o .:? "col"
    col_choice' <- o .:? "col_choice"
    ResponseAnswer
      <$> pure (AnswerId . read <$> row')
      <*> pure (AnswerId . read <$> col')
      <*> pure (AnswerId . read <$> col_choice')
      <*> o .:? "text"
  parseJSON invalid = typeMismatch "ResponseAnswer" invalid


instance FromJSON Respondent where
  parseJSON v@(Object o) = do
    cid' <- o .:? "collector_id"
    Respondent
      <$> parseJSON v
      <*> parseSMDate o "date_start"
      <*> parseSMDate o "date_modified"
      <*> pure (CollectorId . read <$> cid')
      <*> o .:? "status"
  parseJSON invalid = typeMismatch "Respondent" invalid

instance FromJSON ResponseQuestion where
  parseJSON v@(Object o) =
    ResponseQuestion
      <$> parseJSON v
      <*> o .: "answers"
  parseJSON invalid = typeMismatch "ResponseQuestion" invalid


instance FromJSON Response where
  parseJSON v@(Object o) =
    Response
      <$> parseJSON v
      <*> o .: "questions"
  parseJSON invalid = typeMismatch "Response" invalid
