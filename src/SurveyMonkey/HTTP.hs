{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE Trustworthy         #-}

module SurveyMonkey.HTTP (
  -- * types
  ApiKey, Auth,
  -- * the config
  SurveyMonkeyConfig, mkSurveyMonkeyConfig,
  -- * do a call
  Endpoint, SurveyMonkeyResult, SurveyMonkeyError(..), smCallApi,
  -- * helpers
  prefixLog
  ) where

import           Control.Lens.Getter
import           Control.Monad.Catch
import           Control.Monad.IO.Class
import           Control.Monad.Logger
import           Data.Aeson
import           Data.Aeson.Types                (Parser, parseEither,
                                                  typeMismatch)
import qualified Data.Attoparsec.ByteString.Lazy as Atto
import           Data.ByteString.Lazy
import           Data.String
import           Data.Text                       (Text)
import qualified Data.Text                       as T
import           Data.Text.Encoding
import           Network.HTTP.Client             as HTTP
import           Network.HTTP.Client.TLS
import           Network.HTTP.Types.Header
import           SurveyMonkey.Types

prefixLog :: (IsString a, Monoid a) => [a]-> a
prefixLog s = mconcat $ "[ surveyMonkey ] " : s

-- | the HTTP endpoint / path
type Endpoint = Text

-- | generate a 'SurveyMonkeyConfig' from an 'ApiKey' and an 'Auth'
mkSurveyMonkeyConfig :: (MonadLogger m, MonadIO m) =>
                        ApiKey -> Auth -> m SurveyMonkeyConfig
mkSurveyMonkeyConfig k a = do
  $(logDebug) (prefixLog ["creating surveymonkey api"])
  liftIO $ SurveyMonkeyConfig <$> pure k <*> pure a <*> newManager tlsManagerSettings

baseUrl :: Text
baseUrl = "https://api.surveymonkey.net"

apiBaseUrl :: Text
apiBaseUrl = mconcat [baseUrl, "/v2/"]


-- | make a call to the Survey Monkey API
smCallApi ::
  (MonadLogger m, MonadIO m, MonadThrow m, ToJSON args, FromJSON r, Show r) =>
  SurveyMonkeyConfig -> Endpoint -> args -> m (SurveyMonkeyResult r)
smCallApi cfg endpoint args = do
  $(logDebug) (prefixLog ["call arguments", T.pack . show . toJSON $ args])
  r <- smRequest cfg endpoint args >>= smHttpLbsBS cfg
  case r of
    (Left err) -> do
      $(logError) (prefixLog [ "got error on api request: ", T.pack $ show err ])
      return $ Left err
    (Right resp) -> do
      $(logDebug) (prefixLog [ "got result: ", T.pack $ show resp ])
      $(logDebug) (prefixLog ["parsing response"])
      res <- either (return . Left . SurveyMonkeyParseError) return .
             Atto.eitherResult . Atto.parse apiReturnParser . responseBody $ resp
      case res of
        (Left err) ->
          $(logError) (prefixLog [ "parser failure: ", T.pack . show $ err ])
        (Right res') ->
          $(logDebug) (prefixLog [ "parser successful: ", T.pack . show $ res'])

      return res



smRequest :: (MonadThrow m, ToJSON args) =>
             SurveyMonkeyConfig -> Endpoint -> args -> m Request
smRequest cfg endpoint args =
  let hdrs =
        [ (hContentType, "application/json")
        , (hAuthorization, mconcat ["bearer ", encodeUtf8 $ cfg ^. auth])
        ]
  in do
    initReq <- parseUrl . T.unpack . mconcat $
             [apiBaseUrl, endpoint, "?api_key=", cfg ^. apiKey ]
    return $ initReq {
      method = "POST",
      requestHeaders = requestHeaders initReq ++ hdrs,
      requestBody = RequestBodyLBS . encode $ args
      }

smHttpLbsBS :: (MonadLogger m, MonadIO m) => SurveyMonkeyConfig -> Request ->
             m (SurveyMonkeyResult (HTTP.Response ByteString))
smHttpLbsBS cfg rqst =
  let catchOverRate e@(StatusCodeException _ hdrs _) = do
        case read . T.unpack . decodeUtf8 <$> lookup "Retry-After" hdrs of
          Just i -> Just . SurveyMonkeyOverRate $ i
          Nothing -> Just . SurveyMonkeyHTTPException $ e
      catchOverRate e = Just . SurveyMonkeyHTTPException $ e
  in do
    liftIO $ catchJust catchOverRate
     (Right <$> httpLbs rqst (cfg ^. httpManager )) $ \err ->
       return . Left $ err


apiReturnParser :: (FromJSON r) => Atto.Parser (SurveyMonkeyResult r)
apiReturnParser =
  either (Left . SurveyMonkeyParseError) id . parseEither jsonApiReturnParser <$> json


jsonApiReturnParser :: (FromJSON r) => Value -> Parser (SurveyMonkeyResult r)
jsonApiReturnParser (Object v) = do
  status <- v .: "status"
  case status of
    (0 :: Int) -> Right <$> v .: "data"
    1 -> return $ Left SurveyMonkeyNotAuthenticated
    2 -> return $ Left SurveyMonkeyInvalidUserCredentials
    3 -> return $ Left SurveyMonkeyIvalidRequest
    4 -> return $ Left SurveyMonkeyUnknownUser
    5 -> return $ Left SurveyMonkeySystemError
    i -> return $ Left . SurveyMonkeyParseError $ "unexpected api status: " ++ show i
jsonApiReturnParser invalid = typeMismatch "SurveyMonkeyResult" invalid






