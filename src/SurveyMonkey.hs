{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}



module SurveyMonkey (
  -- * configuration
  SurveyMonkeyApi, SurveyMonkeyConfig,
  mkSurveyMonkeyConfig, withSurveyMonkey, withSurveyMonkeyNoLogging,

  -- * paging Requests
  SMList, getPagingList, smList, smListPage, smListPageSize,

  -- * API calls
  -- ** Get Survey List
  SurveyListParams, SurveyIdList, getSurveyList, getPagingSurveyList,

  -- ** Get Survey Details
  SurveyId, SurveyMonkeyResult, getSurveyDetails,

  -- ** Get Survey Collectors
  Collector, CollectorId, CollectorListParams, CollectorList,
  getCollectorList, getPagingCollectorList,

  -- ** Get Respondent Ids
  RespondentId, RespondentListParams, RespondentList,
  getRespondentList, getPagingRespondentList,

  -- ** Get Responses
  Response, ResponsesParams, getResponses

  ) where

import           Control.Concurrent
import           Control.Lens.Operators
import           Control.Monad.Catch
import           Control.Monad.IO.Class
import           Control.Monad.Logger


import qualified Data.Text              as T
import           SurveyMonkey.HTTP
import           SurveyMonkey.Types


type SurveyMonkeyApi m arg res = SurveyMonkeyConfig -> arg -> m (SurveyMonkeyResult res)



getSurveyList :: (MonadLogger m, MonadIO m, MonadThrow m) =>
                 SurveyMonkeyApi m SurveyListParams SurveyIdList
getSurveyList cfg = smCallApi cfg "surveys/get_survey_list"


getSurveyDetails :: (MonadLogger m, MonadIO m, MonadThrow m) =>
                    SurveyMonkeyApi m SurveyId Survey
getSurveyDetails cfg = smCallApi cfg "surveys/get_survey_details"

getCollectorList :: (MonadLogger m, MonadIO m, MonadThrow m) =>
                    SurveyMonkeyApi m CollectorListParams CollectorList
getCollectorList cfg = smCallApi cfg "surveys/get_collector_list"


getRespondentList :: (MonadLogger m, MonadIO m, MonadThrow m) =>
                    SurveyMonkeyApi m RespondentListParams RespondentList
getRespondentList cfg = smCallApi cfg "surveys/get_respondent_list"

getResponses :: (MonadLogger m, MonadIO m, MonadThrow m) =>
                SurveyMonkeyApi m ResponsesParams [Response]
getResponses cfg = smCallApi cfg "surveys/get_responses"


getPagingSurveyList :: (MonadLoggerIO m, MonadThrow m) =>
  SurveyMonkeyConfig -> SurveyListParams -> m [SurveyId]
getPagingSurveyList cfg args = getPagingList cfg getSurveyList args []

getPagingCollectorList :: (MonadLoggerIO m, MonadThrow m) =>
  SurveyMonkeyConfig -> CollectorListParams -> m [Collector]
getPagingCollectorList cfg args = getPagingList cfg getCollectorList args []

getPagingRespondentList :: (MonadLoggerIO m, MonadThrow m) =>
  SurveyMonkeyConfig -> RespondentListParams -> m [Respondent]
getPagingRespondentList cfg args = getPagingList cfg getRespondentList args []



withSurveyMonkey ::
  (MonadLoggerIO m) => Auth -> ApiKey -> (SurveyMonkeyConfig -> m b) -> m b
withSurveyMonkey _auth _api a = do
  cfg <- mkSurveyMonkeyConfig _api _auth
  a cfg

withSurveyMonkeyNoLogging ::
  (MonadIO m) => Auth -> ApiKey -> (SurveyMonkeyConfig -> NoLoggingT m a) -> m a
withSurveyMonkeyNoLogging a b = runNoLoggingT . withSurveyMonkey a b





getPagingList :: (MonadLoggerIO m, MonadThrow m, HasPageMeta args) =>
                 SurveyMonkeyConfig -> SurveyMonkeyApi m args (SMList a) ->
                 args -> [a] -> m [a]
getPagingList cfg action args xs = do
  res <- action cfg args
  case res of
    Left (SurveyMonkeyOverRate i) ->
      let n = 1000000 * i
      in do
        $(logWarn) (prefixLog ["got overrate error, sleeping ", T.pack . show $ i
                              ," seconds "])
        liftIO $ threadDelay n
        getPagingList cfg action args xs

    Left err -> do
      $(logError) (prefixLog ["error fetching Collector List", T.pack $ show err])
      return []
    Right smlist -> do
      let l = smlist ^. smList
          n = length l
          l' = l ++ xs
      if (smlist ^. smListPageSize >= n)
        then return l' else
        let newPage = smlist ^. smListPage + 1
            newargs =
              args &
              pageMetaPage     .~ (Just newPage) &
              pageMetaPageSize .~ (Just $ smlist ^. smListPageSize)
        in do
          $(logDebug) (prefixLog ["fetching next page (", T.pack . show $ newPage, ")"])
          getPagingList cfg action newargs l'

